// [[Rcpp::plugins(cpp11)]]

#include <Rcpp.h>
#include <random>
#include <iostream>
#include <math.h>
//#include <ctime>
using namespace Rcpp;


// [[Rcpp::export]]
NumericVector random_unif_interval(int size, int max_val){
 
 // vector to store the generated random number
 NumericVector index(size);
  
  // initialize the random generator
  std::random_device seed;
  std::default_random_engine rng(seed());
  
  // uncomment this line if std::random_device does not work properly
  //std::default_random_engine rng(time(0));
  
  std::uniform_int_distribution<std::mt19937::result_type> unif_c(1, max_val); 
  for(int i=0; i<size; ++i){
    index[i] = unif_c(rng);
  }
  return(index);
  
}

